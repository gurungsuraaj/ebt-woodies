// class InventoryDetailModel {
//   int statusCode;
//   String key;
//   String locationName;
//   String salesPrice;
//   String inventory;
//   String userRef;

//   InventoryDetailModel({
//     this.statusCode,
//     this.key,
//     this.locationName,
//     this.salesPrice,
//     this.inventory,
//     this.userRef,
//   });
// }

class Autogenerated {
  String odataMetadata;
  List<InventoryDetailModel> value;

  Autogenerated({this.odataMetadata, this.value});

  Autogenerated.fromJson(Map<String, dynamic> json) {
    odataMetadata = json['odata.metadata'];
    if (json['value'] != null) {
      value = new List<InventoryDetailModel>();
      json['value'].forEach((v) {
        value.add(new InventoryDetailModel.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['odata.metadata'] = this.odataMetadata;
    if (this.value != null) {
      data['value'] = this.value.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class InventoryDetailModel {
  String odataEtag;
  String itemNo;
  String locationCode;
  String userRef;
  String locationName;
  String inventory;
  String salesPrice;
  String eTag;

  InventoryDetailModel(
      {this.odataEtag,
      this.itemNo,
      this.locationCode,
      this.userRef,
      this.locationName,
      this.inventory,
      this.salesPrice,
      this.eTag});

  InventoryDetailModel.fromJson(Map<String, dynamic> json) {
    odataEtag = json['odata.etag'];
    itemNo = json['Item_No'];
    locationCode = json['Location_Code'];
    userRef = json['User_Ref'];
    locationName = json['Location_Name'];
    inventory = json['Inventory'];
    salesPrice = json['Sales_Price'];
    eTag = json['ETag'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['odata.etag'] = this.odataEtag;
    data['Item_No'] = this.itemNo;
    data['Location_Code'] = this.locationCode;
    data['User_Ref'] = this.userRef;
    data['Location_Name'] = this.locationName;
    data['Inventory'] = this.inventory;
    data['Sales_Price'] = this.salesPrice;
    data['ETag'] = this.eTag;
    return data;
  }
}
