class PriceCheckModel {
  int statusCode;
  String key;
  String offerNumber;
  String salesPrice;
  String discountedPrice;
  String description;
  String offerType;
  String userRef;

  PriceCheckModel({
    this.statusCode,
    this.key,
    this.offerNumber,
    this.salesPrice,
    this.discountedPrice,
    this.offerType,
    this.description,
    this.userRef,
  });

  PriceCheckModel.fromJson(Map<String, dynamic> json) {
    key = json['Key'];
    offerNumber = json['offer_No'];
    userRef = json['User_Ref'];
    discountedPrice = json['Discounted_Price'];
    offerType = json['Offer_TypeTxt'];
    salesPrice = json['Sales_Price'];
    description = json['Description'];
  }

}
