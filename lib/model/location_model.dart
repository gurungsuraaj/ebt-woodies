class LocationModel {
  int statusCode;
  String key;
  String code;
  String name;
  String replenishmentGroup;
  String defaultWeight;
  String weightPercentage;
  String isUsePlannedCrossDocking;

  LocationModel({
    this.statusCode,
    this.key,
    this.code,
    this.name,
    this.replenishmentGroup,
    this.defaultWeight,
    this.weightPercentage,
    this.isUsePlannedCrossDocking,
  });
}
