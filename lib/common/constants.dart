class Constants {
  static final String userName = 'userName';
  static final String password = 'password';

  static final String server = 'server';
  static final String service = 'service';
  static final String web = 'web';
  static final String company = 'company';
  static final String port = 'port';
  static final String odataPort = 'odataPort';
  static final String ntlmUserName = 'ntlmUserName';
  static final String ntlmPassword = 'ntlmPassword';
  static final String testLoginUserName = 'testLoginUserName';
  static final String testLoginPassword = 'testLoginPassword';
  static final String isConfigured = 'isConfigured';

}