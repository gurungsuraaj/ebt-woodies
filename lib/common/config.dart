class Config {
  static final String baseApiUrl =
      "http://13.127.58.110:2004/MDFPECOMTEST/OData/Company('Woodies')";

  static final String itemListUrl = '$baseApiUrl/ItemList';
  static final String priceCheckUrl = '$baseApiUrl/PricesandOffers';
}