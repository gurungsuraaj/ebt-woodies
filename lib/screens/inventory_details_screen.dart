import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:ebt_woodies/common/config.dart';
import 'package:ebt_woodies/common/constants.dart';
import 'package:ebt_woodies/model/inventory_details_model.dart';
import 'package:ebt_woodies/network/network_operation_manager.dart';
import 'package:ebt_woodies/network/ntlm_client.dart';
import 'package:ebt_woodies/network/rcode.dart';
import 'package:ebt_woodies/network/url_helper.dart';
import 'package:ebt_woodies/utils/SpUtils.dart';
import 'package:ebt_woodies/utils/network_connection.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:ntlm/ntlm.dart';

class InventoryDetailScreen extends StatefulWidget {
  final String barcode;
  final String locaiton;

  const InventoryDetailScreen({Key key, this.barcode, this.locaiton})
      : super(key: key);
  @override
  _InventoryDetailScreenState createState() => _InventoryDetailScreenState();
}

class _InventoryDetailScreenState extends State<InventoryDetailScreen> {
  NTLMClient client;
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = new GlobalKey<FormState>();
  bool isProgressBarShown = false;
  NetworkConnectionCheck networkConnectionCheck = new NetworkConnectionCheck();

  List<InventoryDetailModel> inventoryDetailList = [];

  @override
  void initState() {
    super.initState();
    submitItemInventory();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Inventory Details'),
      ),
      body: ModalProgressHUD(
        inAsyncCall: isProgressBarShown,
        child: SingleChildScrollView(
          child: Center(
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: inventoryDetailList.isEmpty
                  ? Text('No Data Available')
                  : Column(
                      children: [
                        tableTitle(),
                        customTable(),
                      ],
                    ),
            ),
          ),
        ),
      ),
    );
  }

  Widget tableTitle() {
    double size = MediaQuery.of(context).size.width;
    return Container(
      color: Theme.of(context).accentColor,
      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: Table(
        columnWidths: {
          0: FixedColumnWidth(size * 0.5),
          1: FixedColumnWidth(size * 0.25),
          2: FixedColumnWidth(size * 0.2),
        },
        // border: TableBorder.all(color: Colors.black, width: 2),
        border: TableBorder(
          top: BorderSide(color: Colors.black, width: 2),
          right: BorderSide(color: Colors.black, width: 2),
          left: BorderSide(color: Colors.black, width: 2),
          verticalInside: BorderSide(color: Colors.black, width: 2),
        ),
        children: [
          TableRow(
            children: [
              TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Location',
                    softWrap: true,
                    maxLines: 2,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                    ),
                  ),
                ),
              ),
              TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      'Inventory',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
              ),
              TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      'Price',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget customTable() {
    double size = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: Table(
        columnWidths: {
          0: FixedColumnWidth(size * 0.5),
          1: FixedColumnWidth(size * 0.25),
          2: FixedColumnWidth(size * 0.2),
        },
        border: TableBorder.all(color: Colors.black, width: 2),
        children: inventoryDetailList
            .map(
              (inventoryDetail) => TableRow(
                children: [
                  TableCell(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        inventoryDetail.locationName,
                        softWrap: true,
                        maxLines: 2,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  TableCell(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          inventoryDetail.inventory.toString(),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                  TableCell(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          inventoryDetail.salesPrice.toString(),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
            .toList(),
      ),
    );
  }

  void submitItemInventory() async {
    client = NTLM.initializeNTLM(SpUtil.getString(Constants.ntlmUserName),
        SpUtil.getString(Constants.ntlmPassword));
    debugPrint("this is client ${client.username} ${client.password}");
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      showProgressBar();
      await NetworkOperationManager.submitItemInventoryQuery(
              widget.barcode, widget.locaiton, client)
          .then((rs) {
        if (rs.status == Rcode.SUCCESS_CODE) {
          print('*********item inventory submitted***********');
          getInventoryDetails();
        } else if (rs.status == 401) {
          hideProgressBar();
          showSnackBar("Unauthenticated");
        } else {
          hideProgressBar();
          showSnackBar('Error Fetching Location List');
        }
      }).catchError((val) {
        hideProgressBar();
        showSnackBar(val.toString());
      });
    } else {
      showSnackBar("No internet connection");
    }
  }

  void getInventoryDetails() async {
    client = NTLM.initializeNTLM(SpUtil.getString(Constants.ntlmUserName),
        SpUtil.getString(Constants.ntlmPassword));
    debugPrint("this is client ${client.username} ${client.password}");
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      String username = SpUtil.getString(Constants.userName);
      String odataFilter = "?\$filter=User_Ref eq '" + username + "'&\$format=json";
      String url = UrlHelper.getOdataUrl('InventoryDetails') + odataFilter;
      client.get(url).then((res) {
        debugPrint("this is url ----------> $url and ${res.body}");
        var res_code = res.statusCode;

        if (res_code == Rcode.SUCCESS_CODE) {
          hideProgressBar();
          var data = json.decode(res.body);
          var values = data["value"] as List;
          setState(() {
            inventoryDetailList = values
                .map<InventoryDetailModel>(
                    (json) => InventoryDetailModel.fromJson(json))
                .toList();

            if (inventoryDetailList.length == 0) {
              showSnackBar("Pickup list is empty");
            }
          });
        } else {
          hideProgressBar();
          showSnackBar("Something went wrong");
        }
      });
    } else {
      showSnackBar("No internet connection");
    }
  }

  void showProgressBar() {
    setState(() {
      isProgressBarShown = true;
    });
  }

  void hideProgressBar() {
    setState(() {
      isProgressBarShown = false;
    });
  }

  showSnackBar(String snackString) {
    final snackBar = new SnackBar(
        content: Text(snackString),
        duration: Duration(minutes: 5),
        action: SnackBarAction(
          label: "OK",
          onPressed: () {
            _scaffoldKey.currentState.removeCurrentSnackBar();
          },
        ));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
