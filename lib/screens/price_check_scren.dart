import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:ebt_woodies/common/constants.dart';
import 'package:ebt_woodies/model/inventory_details_model.dart';
import 'package:ebt_woodies/model/price_check_model.dart';
import 'package:ebt_woodies/network/network_operation_manager.dart';
import 'package:ebt_woodies/network/ntlm_client.dart';
import 'package:ebt_woodies/network/rcode.dart';
import 'package:ebt_woodies/network/url_helper.dart';
import 'package:ebt_woodies/utils/SpUtils.dart';
import 'package:ebt_woodies/utils/network_connection.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:ntlm/ntlm.dart';

class PriceCheckScreen extends StatefulWidget {
  final String barcode;
  final String locationCode;

  const PriceCheckScreen({Key key, this.barcode, this.locationCode})
      : super(key: key);

  @override
  _PriceCheckScreenState createState() => _PriceCheckScreenState();
}

class _PriceCheckScreenState extends State<PriceCheckScreen> {
  NTLMClient client;
  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = new GlobalKey<FormState>();
  bool isProgressBarShown = false;
  NetworkConnectionCheck networkConnectionCheck = new NetworkConnectionCheck();

  List<PriceCheckModel> priceCheckList = [];

  String salesPrice = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    createDiscountList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('Price Check'),
      ),
      body: ModalProgressHUD(
        inAsyncCall: isProgressBarShown,
        child: Center(
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: EdgeInsets.all(10),
                  child: Text(
                    'Sales Price : $salesPrice',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                tableTitle(),
                priceCheckList.isEmpty || priceCheckList == []
                    ? Container()
                    : customTable(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget tableTitle() {
    double size = MediaQuery.of(context).size.width;
    return Container(
      color: Theme.of(context).accentColor,
      margin: EdgeInsets.fromLTRB(10, 20, 10, 0),
      child: Table(
        columnWidths: {
          0: FixedColumnWidth(size * 0.35),
          1: FixedColumnWidth(size * 0.35),
          2: FixedColumnWidth(size * 0.25),
        },
        // border: TableBorder.all(color: Colors.black, width: 2),
        border: TableBorder(
          top: BorderSide(color: Colors.black, width: 2),
          right: BorderSide(color: Colors.black, width: 2),
          left: BorderSide(color: Colors.black, width: 2),
          verticalInside: BorderSide(color: Colors.black, width: 2),
        ),
        children: [
          TableRow(
            children: [
              TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Offer No',
                    softWrap: true,
                    maxLines: 2,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                    ),
                  ),
                ),
              ),
              TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      'Description',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
              ),
              TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      'Discount Price',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget customTable() {
    double size = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: Table(
        columnWidths: {
          0: FixedColumnWidth(size * 0.35),
          1: FixedColumnWidth(size * 0.35),
          2: FixedColumnWidth(size * 0.25),
        },
        border: TableBorder.all(color: Colors.black, width: 2),
        children: priceCheckList
            .map(
              (inventoryDetail) => TableRow(
                children: [
                  TableCell(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        inventoryDetail.offerNumber,
                        softWrap: true,
                        maxLines: 2,
                        style: TextStyle(fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  TableCell(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          inventoryDetail.description.toString(),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                  TableCell(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Center(
                        child: Text(
                          inventoryDetail.discountedPrice.toString(),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
            .toList(),
      ),
    );
  }

  void createDiscountList() async {
    client = NTLM.initializeNTLM(SpUtil.getString(Constants.ntlmUserName),
        SpUtil.getString(Constants.ntlmPassword));
    debugPrint("this is client ${client.username} ${client.password}");
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      showProgressBar();
      await NetworkOperationManager.createDiscountList(
              widget.barcode, widget.locationCode, client)
          .then((rs) {
        if (rs.status == Rcode.SUCCESS_CODE) {
          salesPrice = rs.responseBody;
          getPriceCheckDetails();
        } else if (rs.status == 401) {
          hideProgressBar();
          priceCheckList = [];
          showSnackBar("Unauthenticated");
        } else {
          hideProgressBar();
          showSnackBar('Error Fetching Location List');
        }
      }).catchError((val) {
        hideProgressBar();
        showSnackBar('Data is not Available');
      });
    } else {
      showSnackBar("No internet connection");
    }
  }

  void getPriceCheckDetails() async {
    client = NTLM.initializeNTLM(SpUtil.getString(Constants.ntlmUserName),
        SpUtil.getString(Constants.ntlmPassword));
    debugPrint("this is client ${client.username} ${client.password}");
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      String username = SpUtil.getString(Constants.userName);
      String odataFilter = "?\$filter=User_Ref eq '" + username + "'&\$format=json";
      String url = UrlHelper.getOdataUrl('PricesandOffers') + odataFilter;
      client.get(url).then((res) {
        debugPrint("this is url ----------> $url and ${res.body}");
        var res_code = res.statusCode;
        if (res_code == Rcode.SUCCESS_CODE) {
          hideProgressBar();
          var data = json.decode(res.body);
          var values = data["value"] as List;
          setState(() {
            priceCheckList = values
                .map<PriceCheckModel>(
                    (json) => PriceCheckModel.fromJson(json))
                .toList();

            if (priceCheckList.length == 0) {
              showSnackBar("Pickup list is empty");
            }
          });
        } else {
          hideProgressBar();
          showSnackBar("Something went wrong");
        }
      });
    } else {
      showSnackBar("No internet connection");
    }
  }

  void showProgressBar() {
    setState(() {
      isProgressBarShown = true;
    });
  }

  void hideProgressBar() {
    setState(() {
      isProgressBarShown = false;
    });
  }

  showSnackBar(String snackString) {
    final snackBar = new SnackBar(
        content: Text(snackString),
        duration: Duration(minutes: 5),
        action: SnackBarAction(
          label: "OK",
          onPressed: () {
            _scaffoldKey.currentState.removeCurrentSnackBar();
          },
        ));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
