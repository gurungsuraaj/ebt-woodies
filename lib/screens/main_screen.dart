import 'package:barcode_scan/model/scan_result.dart';
import 'package:barcode_scan/platform_wrapper.dart';
import 'package:connectivity/connectivity.dart';
import 'package:ebt_woodies/common/constants.dart';
import 'package:ebt_woodies/model/location_model.dart';
import 'package:ebt_woodies/network/network_operation_manager.dart';
import 'package:ebt_woodies/network/ntlm_client.dart';
import 'package:ebt_woodies/network/rcode.dart';
import 'package:ebt_woodies/screens/inventory_details_screen.dart';
import 'package:ebt_woodies/screens/login_screen.dart';
import 'package:ebt_woodies/screens/price_check_scren.dart';
import 'package:ebt_woodies/screens/searchScreen.dart';
import 'package:ebt_woodies/utils/SpUtils.dart';
import 'package:ebt_woodies/utils/network_connection.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:ntlm/ntlm.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  NTLMClient client;
  final _formKey = new GlobalKey<FormState>();
  bool isProgressBarShown = false;
  NetworkConnectionCheck networkConnectionCheck = new NetworkConnectionCheck();

  String barcode = "";
  TextEditingController barCodeController = TextEditingController();

  String selectedLocation;
  List<LocationModel> locationList = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getLocationList();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _exitAlert,
      child: ModalProgressHUD(
        inAsyncCall: isProgressBarShown,
        child: Scaffold(
          key: _scaffoldKey,
          appBar: AppBar(
            title: Text('Check Inventory'),
            actions: [
              IconButton(
                icon: Icon(Icons.search),
                onPressed: () async {
                  String itemNo = await Navigator.push(context,
                      MaterialPageRoute(builder: (context) => SearchScreen()));
                  if (itemNo != null) {
                    barCodeController.text = itemNo;
                  }
                },
              ),
              IconButton(
                icon: Icon(Icons.exit_to_app),
                onPressed: () {
                  _exitAlert('logout');
                },
              ),
            ],
          ),
          body: SingleChildScrollView(
            child: Center(
              child: Container(
                width: MediaQuery.of(context).size.width * 0.85,
                child: Column(
                  children: [
                    SizedBox(
                      height: 20,
                    ),
                    barCodeContainer(),
                    SizedBox(
                      height: 10,
                    ),
                    locationContainer(),
                    SizedBox(
                      height: MediaQuery.of(context).size.height * 0.15,
                    ),
                    buttonsContainer(),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget barCodeContainer() {
    return Container(
      child: Row(
        children: [
          IconButton(
            icon: Icon(
              FontAwesomeIcons.barcode,
              size: 35,
            ),
            onPressed: () {
              barcodeScanning();
              // scanBarcodeNormal();
            },
          ),
          Expanded(
            child: Form(
              key: _formKey,
              child: TextFormField(
                controller: barCodeController,
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please enter the barcode';
                  }
                  return null;
                },
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(
                      width: 1,
                      color: Colors.grey,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(20),
                    borderSide: BorderSide(
                      width: 1,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                  hintText: 'Eg: 584752',
                  hintStyle: TextStyle(
                    color: Color(0xffb8b8b8),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget locationContainer() {
    return Container(
      padding: EdgeInsets.only(
        left: MediaQuery.of(context).size.width * 0.02,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            'Location',
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
              color: Theme.of(context).primaryColor,
            ),
          ),
          Row(
            children: [
              DropdownButton<String>(
                value: selectedLocation,
                style: TextStyle(
                  color: Theme.of(context).primaryColor,
                ),
                underline: Container(
                  height: 2,
                  color: Theme.of(context).primaryColor,
                ),
                onChanged: (String newValue) {
                  setState(() {
                    selectedLocation = newValue;
                  });
                },
                items: locationList.map<DropdownMenuItem<String>>((value) {
                  return DropdownMenuItem<String>(
                    value: value.code,
                    child: Text(value.name),
                  );
                }).toList(),
              ),
              SizedBox(
                width: 5,
              ),
              IconButton(
                icon: Icon(
                  Icons.cancel,
                  color: Theme.of(context).primaryColor,
                ),
                onPressed: () {
                  setState(() {
                    selectedLocation = null;
                  });
                },
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget buttonsContainer() {
    return Column(
      children: [
        Container(
          height: 55,
          width: MediaQuery.of(context).size.width * 0.85,
          child: RaisedButton(
            color: Theme.of(context).primaryColor,
            child: Text(
              'Show Inventory',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            onPressed: () {
              if (_formKey.currentState.validate()) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => InventoryDetailScreen(
                              locaiton: selectedLocation,
                              barcode: barCodeController.text,
                            )));
              }
            },
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
          ),
        ),
        SizedBox(
          height: 20,
        ),
        Container(
          height: 55,
          width: MediaQuery.of(context).size.width * 0.85,
          child: RaisedButton(
            color: Theme.of(context).primaryColor,
            child: Text(
              'Price Check',
              style: TextStyle(
                color: Colors.white,
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
            onPressed: () {
              if (_formKey.currentState.validate()) {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PriceCheckScreen(
                              barcode: barCodeController.text,
                              locationCode: selectedLocation,
                            )));
              }
            },
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(15),
            ),
          ),
        ),
      ],
    );
  }

  logout() {
    SpUtil.putString(Constants.userName, '');
    SpUtil.putString(Constants.password, '');
    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(builder: (context) => LoginScreen()),
        (route) => false);
  }

  Future<bool> _exitAlert([String message]) {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: message == null
                ? Text('Do you want to Exit?')
                : Text('Do you want to Log out?'),
            actions: [
              RaisedButton(
                onPressed: message == null
                    ? () {
                        Navigator.pop(context);
                        Navigator.pop(context);
                      }
                    : () {
                        logout();
                      },
                child: Text('Yes'),
                color: Theme.of(context).primaryColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('No'),
                color: Colors.red,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
            ],
          );
        });
  }

  void getLocationList() async {
    client = NTLM.initializeNTLM(SpUtil.getString(Constants.ntlmUserName),
        SpUtil.getString(Constants.ntlmPassword));
    debugPrint("this is client ${client.username} ${client.password}");
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      showProgressBar();
      await NetworkOperationManager.getLocationList(client).then((rs) {
        if (rs[0].statusCode == Rcode.SUCCESS_CODE) {
          locationList = rs;
          // selectedLocation = locationList[0].code;
          hideProgressBar();
        } else if (rs[0].statusCode == 401) {
          hideProgressBar();
          locationList = [];
          showSnackBar("Unauthenticated");
        } else {
          hideProgressBar();
          showSnackBar('Error Fetching Location List');
        }
      }).catchError((val) {
        hideProgressBar();
        showSnackBar(val.toString());
      });
    } else {
      showSnackBar("No internet connection");
    }
  }

  //scan barcode asynchronously
  Future barcodeScanning() async {
    try {
      ScanResult barcode = await BarcodeScanner.scan();
      setState(() {
        this.barcode = barcode.rawContent;
        barCodeController.text = barcode.rawContent;
      });
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.cameraAccessDenied) {
        setState(() {
          this.barcode = 'No camera permission!';
        });
      } else {
        setState(() => this.barcode = 'Unknown error: $e');
      }
    } on FormatException {
      setState(() => this.barcode = 'Nothing captured.');
    } catch (e) {
      setState(() => this.barcode = 'Unknown error: $e');
    }
  }

  // Future<void> scanBarcodeNormal() async {
  //   String barcodeScanRes;
  //   try {
  //     barcodeScanRes = await FlutterBarcodeScanner.scanBarcode(
  //         "#ff6666", "Cancel", true, ScanMode.BARCODE);
  //     print("This is the result of bar code scanner $barcodeScanRes");
  //     barCodeController.text = barcodeScanRes;
  //   } on PlatformException {
  //     barcodeScanRes = 'Failed to get platform version.';
  //   }
  //   if (!mounted) return;

  //   setState(() {
  //     _scanBarcode = barcodeScanRes;
  //   });
  // }

  void getInventoryDetails() async {
    client = NTLM.initializeNTLM(SpUtil.getString(Constants.ntlmUserName),
        SpUtil.getString(Constants.ntlmPassword));
    debugPrint("this is client ${client.username} ${client.password}");
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      showProgressBar();
      await NetworkOperationManager.submitItemInventoryQuery(
              barCodeController.text, selectedLocation, client)
          .then((rs) {
        if (rs.status == Rcode.SUCCESS_CODE) {
          hideProgressBar();
        } else if (rs.status == 401) {
          hideProgressBar();
          showSnackBar("Unauthenticated");
        } else {
          hideProgressBar();
          showSnackBar(rs.responseBody);
        }
      }).catchError((val) {
        hideProgressBar();
        showSnackBar(val.toString());
      });
    } else {
      showSnackBar("No internet connection");
    }
  }

  void showProgressBar() {
    setState(() {
      isProgressBarShown = true;
    });
  }

  void hideProgressBar() {
    setState(() {
      isProgressBarShown = false;
    });
  }

  showSnackBar(String snackString) {
    final snackBar = new SnackBar(
        content: Text(snackString),
        duration: Duration(minutes: 5),
        action: SnackBarAction(
          label: "OK",
          onPressed: () {
            _scaffoldKey.currentState.removeCurrentSnackBar();
          },
        ));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
