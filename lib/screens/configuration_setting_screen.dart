import 'package:connectivity/connectivity.dart';
import 'package:ebt_woodies/common/constants.dart';
import 'package:ebt_woodies/network/network_operation_manager.dart';
import 'package:ebt_woodies/network/ntlm_client.dart';
import 'package:ebt_woodies/network/rcode.dart';
import 'package:ebt_woodies/utils/SpUtils.dart';
import 'package:ebt_woodies/utils/network_connection.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:ntlm/ntlm.dart';

class ConfigurationSetingScreen extends StatefulWidget {
  @override
  _ConfigurationSetingScreenState createState() =>
      _ConfigurationSetingScreenState();
}

class _ConfigurationSetingScreenState extends State<ConfigurationSetingScreen> {
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  TextEditingController serverController = TextEditingController();
  TextEditingController serviceController = TextEditingController();
  TextEditingController webController = TextEditingController();
  TextEditingController companyController = TextEditingController();
  TextEditingController portController = TextEditingController();
  TextEditingController odataPortController = TextEditingController();
  TextEditingController ntlmUsernameController = TextEditingController();
  TextEditingController ntlmPasswordController = TextEditingController();
  TextEditingController testUserNameController = TextEditingController();
  TextEditingController testPasswordController = TextEditingController();

  FocusNode focusService = FocusNode();
  FocusNode focusWeb = FocusNode();
  FocusNode focusCompany = FocusNode();
  FocusNode focusPort = FocusNode();
  FocusNode focusOdataPort = FocusNode();
  FocusNode foucsNtlmUsername = FocusNode();
  FocusNode focusNtlmPassword = FocusNode();
  FocusNode focusTestUsername = FocusNode();
  FocusNode focusTestPassword = FocusNode();

  bool isProgressBarShown = false;

  NetworkConnectionCheck networkConnectionCheck = new NetworkConnectionCheck();

  NTLMClient client;

  @override
  void initState() {
    super.initState();
    setTextFields();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text('Configuration Settings'),
      ),
      body: ModalProgressHUD(
        inAsyncCall: isProgressBarShown,
        child: SingleChildScrollView(
          child: Center(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.9,
              margin: EdgeInsets.symmetric(vertical: 15),
              child: Column(
                children: [
                  formContainer(),
                  buttonContainer(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget formContainer() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            controller: serverController,
            onFieldSubmitted: (v) {
              FocusScope.of(context).requestFocus(focusPort);
            },
            textInputAction: TextInputAction.next,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please fill the form properly';
              }
              return null;
            },
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              prefixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  child: Icon(
                    Icons.settings_overscan,
                    // size: 40,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Colors.grey,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              labelText: 'Server',
              labelStyle: TextStyle(color: Theme.of(context).primaryColor),
              hintText: 'Eg: 13.271.125',
              hintStyle: TextStyle(color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            controller: portController,
            focusNode: focusPort,
            onFieldSubmitted: (v) {
              FocusScope.of(context).requestFocus(focusOdataPort);
            },
            textInputAction: TextInputAction.next,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please fill the form properly';
              }
              return null;
            },
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              prefixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  child: Icon(
                    Icons.pin_drop,
                    // size: 40,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Colors.grey,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              labelText: 'SOAP Port',
              labelStyle: TextStyle(color: Theme.of(context).primaryColor),
              hintText: 'Eg: 1001',
              hintStyle: TextStyle(color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            controller: odataPortController,
            focusNode: focusOdataPort,
            onFieldSubmitted: (v) {
              FocusScope.of(context).requestFocus(focusService);
            },
            textInputAction: TextInputAction.next,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please fill the form properly';
              }
              return null;
            },
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              prefixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  child: Icon(
                    Icons.pin_drop,
                    // size: 40,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Colors.grey,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              labelText: 'ODATA Port',
              labelStyle: TextStyle(color: Theme.of(context).primaryColor),
              hintText: 'Eg: 1001',
              hintStyle: TextStyle(color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            controller: serviceController,
            focusNode: focusService,
            onFieldSubmitted: (v) {
              FocusScope.of(context).requestFocus(focusWeb);
            },
            textInputAction: TextInputAction.next,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please fill the form properly';
              }
              return null;
            },
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              prefixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  child: Icon(
                    Icons.web,
                    // size: 40,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Colors.grey,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              labelText: 'Service',
              labelStyle: TextStyle(color: Theme.of(context).primaryColor),
              hintText: 'Eg: DynamicsNAV',
              hintStyle: TextStyle(color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            controller: webController,
            focusNode: focusWeb,
            onFieldSubmitted: (v) {
              FocusScope.of(context).requestFocus(focusCompany);
            },
            textInputAction: TextInputAction.next,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please fill the form properly';
              }
              return null;
            },
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              prefixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  child: Icon(
                    Icons.vpn_lock,
                    // size: 40,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Colors.grey,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              labelText: 'WEB',
              labelStyle: TextStyle(color: Theme.of(context).primaryColor),
              // hintText: 'Eg: 13.271.125',
              hintStyle: TextStyle(color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            controller: companyController,
            focusNode: focusCompany,
            onFieldSubmitted: (v) {
              FocusScope.of(context).requestFocus(foucsNtlmUsername);
            },
            textInputAction: TextInputAction.next,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please fill the form properly';
              }
              return null;
            },
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              prefixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  child: Icon(
                    Icons.people,
                    // size: 40,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Colors.grey,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              labelText: 'Company',
              labelStyle: TextStyle(color: Theme.of(context).primaryColor),
              hintText: 'Eg: HRMS',
              hintStyle: TextStyle(color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            controller: ntlmUsernameController,
            focusNode: foucsNtlmUsername,
            onFieldSubmitted: (v) {
              FocusScope.of(context).requestFocus(focusNtlmPassword);
            },
            textInputAction: TextInputAction.next,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please fill the form properly';
              }
              return null;
            },
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              prefixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  child: Icon(
                    Icons.supervised_user_circle,
                    // size: 40,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Colors.grey,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              labelText: 'NTLM Username',
              labelStyle: TextStyle(color: Theme.of(context).primaryColor),
              hintText: 'Eg: ram123',
              hintStyle: TextStyle(color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            obscureText: true,
            controller: ntlmPasswordController,
            focusNode: focusNtlmPassword,
            onFieldSubmitted: (v) {
              FocusScope.of(context).requestFocus(focusTestUsername);
            },
            textInputAction: TextInputAction.next,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please fill the form properly';
              }
              return null;
            },
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              prefixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  child: Icon(
                    Icons.lock_outline,
                    // size: 40,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Colors.grey,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              labelText: 'NTLM Password',
              labelStyle: TextStyle(color: Theme.of(context).primaryColor),
              hintText: 'Eg: ram123',
              hintStyle: TextStyle(color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            controller: testUserNameController,
            focusNode: focusTestUsername,
            onFieldSubmitted: (v) {
              FocusScope.of(context).requestFocus(focusTestPassword);
            },
            textInputAction: TextInputAction.next,
            validator: (value) {
              if (value.isEmpty) {
                return 'Please fill the form properly';
              }
              return null;
            },
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              prefixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  child: Icon(
                    Icons.person,
                    // size: 40,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Colors.grey,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              labelText: 'Test Login Username',
              labelStyle: TextStyle(color: Theme.of(context).primaryColor),
              hintText: 'Eg: ram123',
              hintStyle: TextStyle(color: Theme.of(context).primaryColor),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          TextFormField(
            obscureText: true,
            controller: testPasswordController,
            focusNode: focusTestPassword,
            textInputAction: TextInputAction.done,
            // validator: (value) {
            //   if (value.isEmpty) {
            //     return 'Please fill the form properly';
            //   }
            //   return null;
            // },
            decoration: InputDecoration(
              filled: true,
              fillColor: Colors.white,
              prefixIcon: Padding(
                padding: const EdgeInsets.all(8.0),
                child: CircleAvatar(
                  backgroundColor: Colors.grey[200],
                  child: Icon(
                    Icons.lock,
                    // size: 40,
                    color: Theme.of(context).primaryColor,
                  ),
                ),
              ),
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Colors.grey,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(15),
                borderSide: BorderSide(
                  width: 1,
                  color: Theme.of(context).primaryColor,
                ),
              ),
              labelText: 'Password',
              labelStyle: TextStyle(color: Theme.of(context).primaryColor),
              hintText: 'Eg: ram123',
              hintStyle: TextStyle(color: Theme.of(context).primaryColor),
            ),
          ),
        ],
      ),
    );
  }

  Widget buttonContainer() {
    return Container(
      width: MediaQuery.of(context).size.width * 0.9,
      height: 50,
      margin: EdgeInsets.only(top: 20),
      child: RaisedButton(
        onPressed: () {
          FocusScope.of(context).unfocus();
          saveNtlmCredentialsToPrefs();
          networkConnectionCheck.checkInternet(checkInternetForTestConnection);
        },
        child: Text(
          'Test Connection',
          style: TextStyle(
            color: Colors.white,
            fontSize: 20,
            fontWeight: FontWeight.bold,
          ),
        ),
        color: Theme.of(context).primaryColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
      ),
    );
  }

  checkInternetForTestConnection(bool isInternetConnected) {
    //check the status of the form fields and validate
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      //proceed with the process after the form is validated.
      if (isInternetConnected) {
        //first save the credentials into prefs, then proceed with the processes.
        saveNtlmCredentialsToPrefs().whenComplete(() {
          testConnection();
        });
      } else {
        showSnackBar("No Internet Connection");
      }
    }
  }

  void testConnection() async {
    client = NTLM.initializeNTLM(SpUtil.getString(Constants.ntlmUserName),
        SpUtil.getString(Constants.ntlmPassword));
    debugPrint("this is client ${client.username} ${client.password}");
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      showProgressBar();
      await NetworkOperationManager.testConnection(
              SpUtil.getString(Constants.testLoginUserName),
              SpUtil.getString(Constants.testLoginPassword),
              client)
          .then((rs) {
        if (rs.status == Rcode.SUCCESS_CODE) {
          hideProgressBar();
          SpUtil.putString(Constants.isConfigured, 'true');
          showSnackBar("Connection test successful");
        } else if (rs.status == 401) {
          SpUtil.putString(Constants.isConfigured, 'false');
          hideProgressBar();
          showSnackBar("Unauthenticated");
        } else {
          SpUtil.putString(Constants.isConfigured, 'false');
          hideProgressBar();
          showSnackBar(rs.responseBody);
        }
      }).catchError((val) {
        print("suraj ${val}");
        hideProgressBar();
        SpUtil.putString(Constants.isConfigured, 'false');
        showSnackBar(val.toString());
      });
    } else {
      showSnackBar("No internet connection");
    }
  }

  Future<void> saveNtlmCredentialsToPrefs() async {
    SpUtil.putString(Constants.server, serverController.text);
    SpUtil.putString(Constants.service, serviceController.text);
    SpUtil.putString(Constants.web, webController.text);
    SpUtil.putString(Constants.company, companyController.text);
    SpUtil.putString(Constants.port, portController.text);
    SpUtil.putString(Constants.odataPort, odataPortController.text);
    SpUtil.putString(Constants.ntlmUserName, ntlmUsernameController.text);
    SpUtil.putString(Constants.ntlmPassword, ntlmPasswordController.text);
    SpUtil.putString(Constants.testLoginUserName, testUserNameController.text);
    SpUtil.putString(Constants.testLoginPassword, testPasswordController.text);
    print(SpUtil.getString(Constants.server));
  }

  setTextFields() {
    if (SpUtil.getString(Constants.server).isNotEmpty) {
      showProgressBar();
      serverController.text = SpUtil.getString(Constants.server);
      portController.text = SpUtil.getString(Constants.port);
      odataPortController.text = SpUtil.getString(Constants.odataPort);
      serviceController.text = SpUtil.getString(Constants.service);
      webController.text = SpUtil.getString(Constants.web);
      companyController.text = SpUtil.getString(Constants.company);
      ntlmUsernameController.text = SpUtil.getString(Constants.ntlmUserName);
      ntlmPasswordController.text = SpUtil.getString(Constants.ntlmPassword);
      testUserNameController.text =
          SpUtil.getString(Constants.testLoginUserName);
      testPasswordController.text =
          SpUtil.getString(Constants.testLoginPassword);
      hideProgressBar();
    }
  }

  void showProgressBar() {
    setState(() {
      isProgressBarShown = true;
    });
  }

  void hideProgressBar() {
    setState(() {
      isProgressBarShown = false;
    });
  }

  showSnackBar(String snackString) {
    final snackBar = new SnackBar(
        content: Text(snackString),
        duration: Duration(minutes: 5),
        action: SnackBarAction(
          label: "OK",
          onPressed: () {
            _scaffoldKey.currentState.removeCurrentSnackBar();
          },
        ));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
