import 'package:connectivity/connectivity.dart';
import 'package:ebt_woodies/common/constants.dart';
import 'package:ebt_woodies/network/network_operation_manager.dart';
import 'package:ebt_woodies/network/ntlm_client.dart';
import 'package:ebt_woodies/network/rcode.dart';
import 'package:ebt_woodies/screens/configuration_setting_screen.dart';
import 'package:ebt_woodies/screens/main_screen.dart';
import 'package:ebt_woodies/utils/SpUtils.dart';
import 'package:ebt_woodies/utils/network_connection.dart';
import 'package:flutter/material.dart';
import 'package:ntlm/ntlm.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController userNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  FocusNode focusPassword = FocusNode();

  NTLMClient client;

  var _scaffoldKey = new GlobalKey<ScaffoldState>();
  final _formKey = new GlobalKey<FormState>();
  bool isProgressBarShown = false;
  NetworkConnectionCheck networkConnectionCheck = new NetworkConnectionCheck();

  @override
  void initState() {
    super.initState();
    setLoginCredentials();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: loginContainer(),
        ),
      ),
    );
  }

  Widget backgroundImage() {
    return Container(
      height: MediaQuery.of(context).size.height * 0.7,
      width: MediaQuery.of(context).size.width * 0.9,
      decoration: BoxDecoration(
        image: DecorationImage(image: AssetImage('images/woodies.png')),
      ),
    );
  }

  Widget loginContainer() {
    return Center(
      child: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width * 0.9,
        padding: EdgeInsets.only(bottom: 20),
        child: Column(
          children: [
            Container(
              height: MediaQuery.of(context).size.height * 0.45,
              // width: MediaQuery.of(context).size.width * 0.9,
              decoration: BoxDecoration(
                image: DecorationImage(image: AssetImage('images/woodies.png')),
              ),
            ),
            // Spacer(),
            Container(
              // width: MediaQuery.of(context).size.width * 0.9,
              decoration: BoxDecoration(
                color: Colors.grey[200],
                borderRadius: BorderRadius.circular(15),
              ),
              padding: EdgeInsets.symmetric(vertical: 30, horizontal: 30),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.75,
                      child: TextFormField(
                        controller: userNameController,
                        onFieldSubmitted: (v) {
                          FocusScope.of(context).requestFocus(focusPassword);
                        },
                        textInputAction: TextInputAction.next,
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Please fill the form properly';
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          prefixIcon: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: CircleAvatar(
                              backgroundColor: Colors.grey[200],
                              child: Icon(
                                Icons.person,
                                // size: 40,
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide(
                              width: 1,
                              color: Colors.grey,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide(
                              width: 1,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                          labelText: 'Username',
                          labelStyle:
                              TextStyle(color: Theme.of(context).primaryColor),
                          hintText: 'Eg: ram123',
                          hintStyle:
                              TextStyle(color: Theme.of(context).primaryColor),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.75,
                      child: TextFormField(
                        controller: passwordController,
                        focusNode: focusPassword,
                        textInputAction: TextInputAction.done,
                        obscureText: true,
                        validator: (value) {
                          // if (value.isEmpty) {
                          //   return 'Please fill the form properly';
                          // }
                          // return null;
                        },
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          prefixIcon: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: CircleAvatar(
                              backgroundColor: Colors.grey[200],
                              child: Icon(
                                Icons.lock,
                                // size: 40,
                                color: Theme.of(context).primaryColor,
                              ),
                            ),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide(
                              width: 1,
                              color: Colors.grey,
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(15),
                            borderSide: BorderSide(
                              width: 1,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                          labelText: 'Password',
                          labelStyle:
                              TextStyle(color: Theme.of(context).primaryColor),
                          hintText: 'Eg: 584752',
                          hintStyle:
                              TextStyle(color: Theme.of(context).primaryColor),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) =>
                                    ConfigurationSetingScreen()));
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            'Configuration Setting',
                            style: TextStyle(
                                color: Theme.of(context).primaryColor,
                                fontSize: 16,
                                fontWeight: FontWeight.bold),
                          ),
                          Icon(
                            Icons.settings,
                            color: Theme.of(context).primaryColor,
                            size: 20,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: 20,
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.9,
              height: 50,
              child: RaisedButton(
                onPressed: () {
                  if (SpUtil.getString(Constants.isConfigured) != 'true') {
                    showSnackBar(
                        'First configure the application and try again');
                  } else if (_formKey.currentState.validate()) {
                    networkConnectionCheck
                        .checkInternet(checkInternetForTestConnection);
                  } else {
                    showSnackBar('Please Fill the Form Properly');
                  }
                },
                child: Text(
                  'Login : Window ID',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                color: Theme.of(context).primaryColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Container(
              width: MediaQuery.of(context).size.width * 0.9,
              height: 50,
              child: RaisedButton(
                onPressed: () {
                  if (SpUtil.getString(Constants.isConfigured) != 'true') {
                    showSnackBar(
                        'First configure the application and try again');
                  } else if (_formKey.currentState.validate()) {
                    networkConnectionCheck
                        .checkInternet(checkInternetForTestConnection);
                  } else {
                    showSnackBar('Please Fill the Form Properly');
                  }
                },
                child: Text(
                  'Login : Staff ID',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 20,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                color: Theme.of(context).primaryColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  checkInternetForTestConnection(bool isInternetConnected) {
    //check the status of the form fields and validate
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();

      //proceed with the process after the form is validated.
      if (isInternetConnected) {
        //first save the credentials into prefs, then proceed with the processes.
        testConnection();
      } else {
        showSnackBar("No Internet Connection");
      }
    }
  }

  void testConnection() async {
    client = NTLM.initializeNTLM(SpUtil.getString(Constants.ntlmUserName),
        SpUtil.getString(Constants.ntlmPassword));
    debugPrint("this is client ${client.username} ${client.password}");
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      showProgressBar();
      await NetworkOperationManager.login(
              userNameController.text, passwordController.text, client)
          .then((rs) {
        if (rs.status == Rcode.SUCCESS_CODE) {
          hideProgressBar();
          Navigator.pushAndRemoveUntil(
              context,
              MaterialPageRoute(builder: (context) => MainScreen()),
              (Route<dynamic> route) => false);
          saveLoginCredentials();
        } else if (rs.status == 401) {
          hideProgressBar();
          showSnackBar("Unauthenticated");
        } else {
          hideProgressBar();
          showSnackBar(rs.responseBody);
        }
      }).catchError((val) {
        hideProgressBar();
        showSnackBar(val.toString());
      });
    } else {
      showSnackBar("No internet connection");
    }
  }

  saveLoginCredentials() {
    SpUtil.putString(Constants.userName, userNameController.text);
    SpUtil.putString(Constants.password, passwordController.text);
  }

  setLoginCredentials() {
    if (SpUtil.getString(Constants.userName).isNotEmpty) {
      userNameController.text = SpUtil.getString(Constants.userName);
      passwordController.text = SpUtil.getString(Constants.password);
    }
  }

  void showProgressBar() {
    setState(() {
      isProgressBarShown = true;
    });
  }

  void hideProgressBar() {
    setState(() {
      isProgressBarShown = false;
    });
  }

  showSnackBar(String snackString) {
    final snackBar = new SnackBar(
        content: Text(snackString),
        duration: Duration(minutes: 5),
        action: SnackBarAction(
          label: "OK",
          onPressed: () {
            _scaffoldKey.currentState.removeCurrentSnackBar();
          },
        ));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
