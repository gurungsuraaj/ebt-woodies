import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:ebt_woodies/common/config.dart';
import 'package:ebt_woodies/common/constants.dart';
import 'package:ebt_woodies/model/Item_detail_model.dart';
import 'package:ebt_woodies/network/network_operation_manager.dart';
import 'package:ebt_woodies/network/ntlm_client.dart';
import 'package:ebt_woodies/network/rcode.dart';
import 'package:ebt_woodies/network/url_helper.dart';
import 'package:ebt_woodies/utils/SpUtils.dart';
import 'package:ebt_woodies/utils/network_connection.dart';
import 'package:flutter/material.dart';
import 'package:modal_progress_hud/modal_progress_hud.dart';
import 'package:ntlm/ntlm.dart';

class SearchScreen extends StatefulWidget {
  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  NTLMClient client;
  final _formKey = new GlobalKey<FormState>();
  bool isProgressBarShown = false;
  NetworkConnectionCheck networkConnectionCheck = new NetworkConnectionCheck();

  List<ItemDetailModel> itemList, searchResultList = [];

  TextEditingController searchController = TextEditingController();
  FocusNode focusSearch = FocusNode();
  String selectedItemCode = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return ModalProgressHUD(
      inAsyncCall: isProgressBarShown,
      child: Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          title: Text("Search Item"),
        ),
        body: Center(
          child: Container(
            height: MediaQuery.of(context).size.height,
            margin: EdgeInsets.only(top: 10),
            child: ListView(
              children: [
                searchContainer(),
                SizedBox(height: 10),
                searchResultList.isEmpty
                    ? Center(child: Text('No data'))
                    : Column(children: [
                        titleContainer(),
                        searchResult(),
                      ])
              ],
            ),
          ),
        ),
        floatingActionButton: selectedItemCode != ''
            ? FloatingActionButton(
                backgroundColor: Theme.of(context).primaryColor,
                onPressed: () {
                  Navigator.pop(context, selectedItemCode);
                },
                child: Text('OK'),
              )
            : Container(),
      ),
    );
  }

  Widget searchContainer() {
    return Container(
      margin: EdgeInsets.symmetric(
          horizontal: MediaQuery.of(context).size.width * 0.05),
      width: MediaQuery.of(context).size.width * 0.9,
      child: Row(
        children: [
          Expanded(
            child: Form(
              key: _formKey,
              child: TextFormField(
                style: TextStyle(fontSize: 20),
                textCapitalization: TextCapitalization.characters,
                decoration: InputDecoration(
                  labelText: 'Search',
                ),
                controller: searchController,
                onChanged: (text) {},
                validator: (value) {
                  if (value.isEmpty) {
                    return 'Please fill this field';
                  }
                  return null;
                },
              ),
            ),
          ),
          RaisedButton(
            onPressed: () {
              // showLoadMore = true;
              if (_formKey.currentState.validate()) {
                FocusScope.of(context).requestFocus(new FocusNode());
                var text = searchController.text;
                debugPrint("this is text ===> $text");
                if (text.isNotEmpty) {
                  if (text.contains(new RegExp(r'[A-Za-z]'))) {
                    geItemList('name');
                  } else {
                    geItemList('number');
                  }
                }
              }
            },
            color: Theme.of(context).primaryColor,
            child: Text(
              'Search',
              style: TextStyle(
                color: Colors.white,
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget searchTextField() {
    return Container(
      height: 40,
      child: TextField(
        focusNode: focusSearch,
        autofocus: true,
        controller: searchController,
        decoration: InputDecoration(
          fillColor: Colors.white,
          filled: true,
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              width: 1,
              color: Colors.grey,
            ),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(20),
            borderSide: BorderSide(
              width: 1,
              color: Theme.of(context).primaryColor,
            ),
          ),
          hintText: 'Eg: 584752',
          hintStyle: TextStyle(
            color: Theme.of(context).primaryColor,
          ),
        ),
        onChanged: (value) {
          searchResultList = [];
          print('this is the value $value');
          setState(() {
            if (value.isNotEmpty) {
              itemList.forEach(
                (element) {
                  if (element.no.contains(value.toUpperCase()) ||
                      element.description.contains(value.toUpperCase())) {
                    searchResultList.add(element);
                    print(element.no);
                  }
                },
              );
            }
          });
        },
      ),
    );
  }

  Widget titleContainer() {
    double size = MediaQuery.of(context).size.width;
    return Container(
      color: Theme.of(context).primaryColor,
      child: Table(
        columnWidths: {
          0: FixedColumnWidth(size * 0.3),
          1: FixedColumnWidth(size * 0.65),
        },
        // border: TableBorder.all(color: Colors.black, width: 2),
        border: TableBorder(
          top: BorderSide(color: Colors.black, width: 2),
          right: BorderSide(color: Colors.black, width: 2),
          left: BorderSide(color: Colors.black, width: 2),
          verticalInside: BorderSide(color: Colors.black, width: 2),
        ),
        children: [
          TableRow(
            children: [
              TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    'Item No',
                    softWrap: true,
                    maxLines: 2,
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 17,
                    ),
                  ),
                ),
              ),
              TableCell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      'Description',
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                        fontSize: 17,
                      ),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget searchResult() {
    double size = MediaQuery.of(context).size.width;
    return Container(
      margin: EdgeInsets.fromLTRB(10, 0, 10, 0),
      child: Table(
        columnWidths: {
          0: FixedColumnWidth(size * 0.3),
          1: FixedColumnWidth(size * 0.65),
        },
        border: TableBorder.all(color: Colors.black, width: 2),
        children: searchResultList
            .map(
              (searchItem) => TableRow(
                key: Key(searchItem.no),
                decoration: BoxDecoration(
                  color: selectedItemCode == searchItem.no
                      ? Colors.lightGreen
                      : Colors.white,
                ),
                children: [
                  TableCell(
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          if (selectedItemCode == searchItem.no) {
                            selectedItemCode = '';
                          } else {
                            selectedItemCode = searchItem.no;
                          }
                        });
                      },
                      child: Container(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          searchItem.no,
                          softWrap: true,
                          maxLines: 2,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                  ),
                  TableCell(
                    child: InkWell(
                      onTap: () {
                        setState(() {
                          if (selectedItemCode == searchItem.no) {
                            selectedItemCode = '';
                          } else {
                            selectedItemCode = searchItem.no;
                          }
                        });
                      },
                      child: Container(
                        padding: const EdgeInsets.all(8.0),
                        child: Center(
                          child: Text(
                            searchItem.description.toString(),
                            style: TextStyle(fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            )
            .toList(),
      ),
    );
  }

  selectItem([String message]) {
    return showDialog(
        barrierDismissible: false,
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Do you want to select this item?'),
            actions: [
              RaisedButton(
                onPressed: () {
                  Navigator.pop(context);
                  Navigator.pop(context);
                },
                child: Text('Yes'),
                color: Theme.of(context).primaryColor,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
              RaisedButton(
                onPressed: () {
                  Navigator.pop(context);
                },
                child: Text('No'),
                color: Colors.red,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(5),
                ),
              ),
            ],
          );
        });
  }

  void geItemList(String mode) async {
    String filterUrl;
    if (mode == 'name') {
      filterUrl = "?\$filter=substringof(Description,'" +
          searchController.text.toUpperCase() +
          "')&\$format=json";
    } else if (mode == 'number') {
      filterUrl = "?\$filter=substringof(No,'" +
          searchController.text +
          "')&\$format=json";
    }
    var url = UrlHelper.getOdataUrl('ItemList') + filterUrl;
    client = NTLM.initializeNTLM(SpUtil.getString(Constants.ntlmUserName),
        SpUtil.getString(Constants.ntlmPassword));
    debugPrint("this is client ${client.username} ${client.password}");
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      showProgressBar();
      client.get(url).then((res) {
        debugPrint("this is url ----------> $url and ${res.body}");
        var res_code = res.statusCode;

        if (res_code == Rcode.SUCCESS_CODE) {
          hideProgressBar();
          var data = json.decode(res.body);
          var values = data["value"] as List;
          setState(() {
            searchResultList = values
                .map<ItemDetailModel>((json) => ItemDetailModel.fromJson(json))
                .toList();

            if (searchResultList.length == 0) {
              showSnackBar("Pickup list is empty");
            }
          });
        } else {
          hideProgressBar();
          showSnackBar("Something went wrong");
        }
      });
    }
  }

  void showProgressBar() {
    setState(() {
      isProgressBarShown = true;
    });
  }

  void hideProgressBar() {
    setState(() {
      isProgressBarShown = false;
    });
  }

  showSnackBar(String snackString) {
    final snackBar = new SnackBar(
        content: Text(snackString),
        duration: Duration(minutes: 5),
        action: SnackBarAction(
          label: "OK",
          onPressed: () {
            _scaffoldKey.currentState.removeCurrentSnackBar();
          },
        ));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
