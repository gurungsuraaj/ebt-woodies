import 'package:ebt_woodies/common/constants.dart';
import 'package:ebt_woodies/screens/login_screen.dart';
import 'package:ebt_woodies/screens/main_screen.dart';
import 'package:ebt_woodies/utils/SpUtils.dart';
import 'package:flutter/material.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SpUtil.getInstance();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Woodies App',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        primaryColor: Color(0xff4a722b),
        accentColor: Color(0xff6c9b39),
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: SpUtil.getString(Constants.userName).isEmpty
          ? LoginScreen()
          : MainScreen(),
    );
  }
}
