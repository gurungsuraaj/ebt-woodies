import 'dart:convert';

import 'package:ebt_woodies/common/config.dart';
import 'package:ebt_woodies/common/constants.dart';
import 'package:ebt_woodies/model/Item_detail_model.dart';
import 'package:ebt_woodies/model/inventory_details_model.dart';
import 'package:ebt_woodies/model/location_model.dart';
import 'package:ebt_woodies/model/network_response_model.dart';
import 'package:ebt_woodies/model/price_check_model.dart';
import 'package:ebt_woodies/network/rcode.dart';
import 'package:ebt_woodies/network/url_helper.dart';
import 'package:ebt_woodies/utils/SpUtils.dart';
import 'package:ntlm/ntlm.dart';
import 'package:xml/xml.dart' as xml;
import 'package:xml2json/xml2json.dart';

class NetworkOperationManager {
  static Future<NetworkResponse> testConnection(
      String userName, String passWord, NTLMClient client) async {
    NetworkResponse rs = new NetworkResponse();
    var url = UrlHelper.getSOAPUrl("InventoryAppServices");

    String response = "";
    print("This is the url $url");
    print("This is the helper ${UrlHelper.getSOAPUrl("WebProcess")}");
    var envelope =
        '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:microsoft-dynamics-schemas/codeunit/InventoryAppServices">
<soapenv:Body>
<urn:LogInHHTUser>
<urn:useRef>$userName</urn:useRef>
<urn:passwordtxt>$passWord</urn:passwordtxt>
</urn:LogInHHTUser>
</soapenv:Body>
</soapenv:Envelope>''';
    print("$envelope");
    await client
        .post(
      url,
      headers: {
        "Content-Type": "text/xml",
        "Accept-Charset": "utf-8",
        "SOAPAction":
            "urn:microsoft-dynamics-schemas/codeunit/InventoryAppServices",
      },
      body: envelope,
      encoding: Encoding.getByName("UTF-8"),
    )
        .then((res) {
      print("This is the response ${res.body} ${res.statusCode}");
      var rawXmlResponse = res.body;
      xml.XmlDocument parsedXml = xml.parse(rawXmlResponse);

      if (res.statusCode == Rcode.SUCCESS_CODE) {
        var resValue = parsedXml.findAllElements("return_value");
        resValue.length < 1
            ? " "
            : response = (resValue.map((node) => node.text)).first;

        rs.responseBody = response;
        rs.status = res.statusCode;
      } else {
        var resValue = parsedXml.findAllElements("faultstring");
        resValue.length < 1
            ? " "
            : response = (resValue.map((node) => node.text)).first;

        rs.responseBody = response;
        rs.status = res.statusCode;
      }
    });

    return rs;
  }

  static Future<NetworkResponse> login(
      String userName, String passWord, NTLMClient client) async {
    NetworkResponse rs = new NetworkResponse();
    var url = UrlHelper.getSOAPUrl("InventoryAppServices");

    String response = "";
    print("This is the url $url");
    print("This is the helper ${UrlHelper.getSOAPUrl("InventoryAppServices")}");
    var envelope =
        '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:microsoft-dynamics-schemas/codeunit/InventoryAppServices">
<soapenv:Body>
<urn:LogInHHTUser>
<urn:useRef>$userName</urn:useRef>
<urn:passwordtxt>$passWord</urn:passwordtxt>
</urn:LogInHHTUser>
</soapenv:Body>
</soapenv:Envelope>''';
    print("$envelope");
    await client
        .post(
      url,
      headers: {
        "Content-Type": "text/xml",
        "Accept-Charset": "utf-8",
        "SOAPAction":
            "urn:microsoft-dynamics-schemas/codeunit/InventoryAppServices",
      },
      body: envelope,
      encoding: Encoding.getByName("UTF-8"),
    )
        .then((res) {
      print("This is the response ${res.body} ${res.statusCode}");
      var rawXmlResponse = res.body;
      xml.XmlDocument parsedXml = xml.parse(rawXmlResponse);

      if (res.statusCode == Rcode.SUCCESS_CODE) {
        var resValue = parsedXml.findAllElements("return_value");
        resValue.length < 1
            ? " "
            : response = (resValue.map((node) => node.text)).first;

        rs.responseBody = response;
        rs.status = res.statusCode;
      } else {
        var resValue = parsedXml.findAllElements("faultstring");
        resValue.length < 1
            ? " "
            : response = (resValue.map((node) => node.text)).first;

        rs.responseBody = response;
        rs.status = res.statusCode;
      }
    });

    return rs;
  }

  static Future<NetworkResponse> submitItemInventoryQuery(
      String barcode, String location, NTLMClient client) async {
    location = location == null ? '' : location;
    NetworkResponse rs = new NetworkResponse();
    var url = UrlHelper.getSOAPUrl("InventoryAppServices");
    String username = SpUtil.getString(Constants.userName);
    String response = "";
    print("This is the url $url");
    print("This is the helper ${UrlHelper.getSOAPUrl("WebProcess")}");
    var envelope =
        '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:microsoft-dynamics-schemas/codeunit/InventoryAppServices">
    <soapenv:Body>
        <urn:SearchandUpdateScanned>
            <urn:userref>$username</urn:userref>
            <urn:barcodeScanned>$barcode</urn:barcodeScanned>
            <urn:locationP>$location</urn:locationP>
        </urn:SearchandUpdateScanned>
    </soapenv:Body>
</soapenv:Envelope>''';
    print("$envelope");
    await client
        .post(
      url,
      headers: {
        "Content-Type": "text/xml",
        "Accept-Charset": "utf-8",
        "SOAPAction":
            "urn:microsoft-dynamics-schemas/codeunit/InventoryAppServices",
      },
      body: envelope,
      encoding: Encoding.getByName("UTF-8"),
    )
        .then((res) {
      print("This is the response ${res.body} ${res.statusCode}");
      var rawXmlResponse = res.body;
      xml.XmlDocument parsedXml = xml.parse(rawXmlResponse);

      if (res.statusCode == Rcode.SUCCESS_CODE) {
        var resValue = parsedXml.findAllElements("return_value");
        resValue.length < 1
            ? " "
            : response = (resValue.map((node) => node.text)).first;

        rs.responseBody = response;
        rs.status = res.statusCode;
      } else {
        var resValue = parsedXml.findAllElements("faultstring");
        resValue.length < 1
            ? " "
            : response = (resValue.map((node) => node.text)).first;

        rs.responseBody = response;
        rs.status = res.statusCode;
      }
    });

    return rs;
  }

  static Future<NetworkResponse> createDiscountList(
      String barcode, String storeCode, NTLMClient client) async {
    NetworkResponse rs = new NetworkResponse();
    var url = UrlHelper.getSOAPUrl("InventoryAppServices");
    String username = SpUtil.getString(Constants.userName);
    String response = "";
    print("This is the url $url");
    print("This is the helper ${UrlHelper.getSOAPUrl("WebProcess")}");
    var envelope =
        '''<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:urn="urn:microsoft-dynamics-schemas/codeunit/InventoryAppServices">
    <soapenv:Body>
        <urn:CreateTempDiscountList>
            <urn:userref>$username</urn:userref>
            <urn:itemCode>$barcode</urn:itemCode>
            <urn:storeCode>$storeCode</urn:storeCode>
            <urn:salesPricev></urn:salesPricev>
        </urn:CreateTempDiscountList>
    </soapenv:Body>
</soapenv:Envelope>''';
    print("$envelope");
    await client
        .post(
      url,
      headers: {
        "Content-Type": "text/xml",
        "Accept-Charset": "utf-8",
        "SOAPAction":
            "urn:microsoft-dynamics-schemas/codeunit/InventoryAppServices",
      },
      body: envelope,
      encoding: Encoding.getByName("UTF-8"),
    )
        .then((res) {
      print("This is the response ${res.body} ${res.statusCode}");
      var rawXmlResponse = res.body;
      xml.XmlDocument parsedXml = xml.parse(rawXmlResponse);

      if (res.statusCode == Rcode.SUCCESS_CODE) {
        var resValue = parsedXml.findAllElements("salesPricev");
        resValue.length < 1
            ? " "
            : response = (resValue.map((node) => node.text)).first;

        rs.responseBody = response;
        rs.status = res.statusCode;
      } else {
        var resValue = parsedXml.findAllElements("faultstring");
        resValue.length < 1
            ? " "
            : response = (resValue.map((node) => node.text)).first;

        rs.responseBody = response;
        rs.status = res.statusCode;
      }
    });
    print('this is the sales price value ${rs.responseBody}');
    return rs;
  }

  static Future<List<LocationModel>> getLocationList(NTLMClient client) async {
    NetworkResponse rs = new NetworkResponse();
    var url = UrlHelper.getPAGEUrl("LocationList");
    List<LocationModel> locationArrayList = new List();
    Xml2Json xml2json = new Xml2Json();
    String response = "";
    print("This is the url $url");
    print("This is the helper ${UrlHelper.getPAGEUrl("WebProcess")}");
    var envelope =
        '''<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns1="urn:microsoft-dynamics-schemas/page/locationlist">
    <soap:Header></soap:Header>
    <soap:Body>
        <tns1:ReadMultiple>
            <tns1:filter>
                <tns1:Field></tns1:Field>
                <tns1:Criteria></tns1:Criteria>
            </tns1:filter>
            <tns1:bookmarkKey></tns1:bookmarkKey>
            <tns1:setSize></tns1:setSize>
        </tns1:ReadMultiple>
    </soap:Body>
</soap:Envelope>''';
    print("$envelope");
    await client
        .post(
      url,
      headers: {
        "Content-Type": "text/xml",
        "Accept-Charset": "utf-8",
        "SOAPAction": "urn:microsoft-dynamics-schemas/page/locationlist",
      },
      body: envelope,
      encoding: Encoding.getByName("UTF-8"),
    )
        .then((res) {
      print("This is the response ${res.body} ${res.statusCode}");
      var rawXmlResponse = res.body;
      xml.XmlDocument parsedXml = xml.parse(rawXmlResponse);

      if (res.statusCode == Rcode.SUCCESS_CODE) {
        parsedXml.findAllElements("LocationList").forEach((val) {
          LocationModel locationList = new LocationModel();
          // print("This is loop $val");
          xml2json.parse(val.toString());
          var json = xml2json.toParker();
          var data = jsonDecode(json);
          locationList.key = data["LocationList"]["Key"] ?? "";
          locationList.code = data["LocationList"]["Code"] ?? "";
          locationList.name = data["LocationList"]["Name"] ?? "";
          locationList.defaultWeight =
              data["LocationList"]["Default_Weight"] ?? "";
          locationList.weightPercentage =
              data["LocationList"]["WeightPercentage"] ?? "";
          locationList.replenishmentGroup =
              data["LocationList"]["Replenishment_Group"] ?? "";
          locationList.isUsePlannedCrossDocking =
              data["LocationList"]["Use_Planned_Cross_Docking"] ?? "";

          locationList.statusCode = res.statusCode;
          print('this is the name of the location ');
          print(
              'this is the name of the location ${data["LocationList"]["Name"]}');
          locationArrayList.add(locationList);
        });
      } else {
        rs.responseBody = response;
        rs.status = res.statusCode;
      }
    });

    return locationArrayList;
  }

  // static Future<List<ItemDetailModel>> getItemList(
  //     NTLMClient client, String filterUrl) async {
  //   NetworkResponse rs = new NetworkResponse();
  //   var url = Config.itemListUrl + filterUrl;
  //   print(url);
  //   List<ItemDetailModel> itemArrayList = new List();
  //   Xml2Json xml2json = new Xml2Json();
  //   String response = "";
  //   await client.get(
  //     url,
  //     headers: {
  //       "Content-Type": "text/xml",
  //     },
  //   ).then((res) {
  //     print("This is the response ${res.statusCode} ${res.body} ");
  //     var rawXmlResponse = res.body;
  //     xml.XmlDocument parsedXml = xml.parse(rawXmlResponse);
  //     if (res.statusCode == Rcode.SUCCESS_CODE) {
  //       parsedXml.findAllElements("ItemList").forEach((val) {
  //         ItemDetailModel itemList = new ItemDetailModel();
  //         xml2json.parse(val.toString().replaceAll(r'\', r'\'));
  //         var json = xml2json.toParker();
  //         var data = jsonDecode(json);
  //         itemList.no = data["ItemList"]["No"] ?? "";
  //         itemList.description = data["ItemList"]["Description"] ?? "";
  //         // itemList.statusCode = res.statusCode;
  //         print('this is the name of the item ${data["itemList"]["No"]}');
  //         itemArrayList.add(itemList);
  //       });
  //     } else {
  //       rs.responseBody = response;
  //       rs.status = res.statusCode;
  //     }
  //   });

  //   return itemArrayList;
  // }

//   static Future<List<InventoryDetailModel>> getInventoryDetailsList(
//       NTLMClient client) async {
//     NetworkResponse rs = new NetworkResponse();
//     var url = UrlHelper.getPAGEUrl("InventoryDetails");
//     List<InventoryDetailModel> inventoryDetailsArrayList = new List();
//     Xml2Json xml2json = new Xml2Json();
//     String username =
//         SpUtil.getString(Constants.userName).replaceAll(r'\', r'\\');
//     String response = "";
//     print("This is the url $url");
//     print("This is the helper ${UrlHelper.getPAGEUrl("WebProcess")}");
//     var envelope =
//         '''<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:tns1="urn:microsoft-dynamics-schemas/page/inventorydetails">
//     <soap:Header></soap:Header>
//     <soap:Body>
//         <tns1:ReadMultiple>
//             <tns1:filter>
//                 <tns1:Field>User_Ref</tns1:Field>
//                 <tns1:Criteria>$username</tns1:Criteria>
//             </tns1:filter>
//             <tns1:bookmarkKey></tns1:bookmarkKey>
//             <tns1:setSize></tns1:setSize>
//         </tns1:ReadMultiple>
//     </soap:Body>
// </soap:Envelope>''';
//     print("$envelope");
//     await client
//         .post(
//       url,
//       headers: {
//         "Content-Type": "text/xml",
//         "Accept-Charset": "utf-8",
//         "SOAPAction": "urn:microsoft-dynamics-schemas/page/inventorydetails",
//       },
//       body: envelope,
//       encoding: Encoding.getByName("UTF-8"),
//     )
//         .then((res) {
//       print("This is the response ${res.body} ${res.statusCode}");
//       var rawXmlResponse = res.body;
//       xml.XmlDocument parsedXml = xml.parse(rawXmlResponse);

//       if (res.statusCode == Rcode.SUCCESS_CODE) {
//         parsedXml.findAllElements("InventoryDetails").forEach((val) {
//           InventoryDetailModel inventoryDetailsList =
//               new InventoryDetailModel();
//           // print("This is loop $val");
//           xml2json.parse(val.toString());
//           var json = xml2json.toParker();
//           var data = jsonDecode(json);
//           inventoryDetailsList.key = data["InventoryDetails"]["Key"] ?? "";
//           inventoryDetailsList.locationName =
//               data["InventoryDetails"]["Location_Name"] ?? "";
//           inventoryDetailsList.inventory =
//               data["InventoryDetails"]["Inventory"] ?? "";
//           inventoryDetailsList.salesPrice =
//               data["InventoryDetails"]["Sales_Price"] ?? "";
//           inventoryDetailsList.userRef =
//               data["InventoryDetails"]["User_Ref"] ?? "";

//           inventoryDetailsList.statusCode = res.statusCode;
//           inventoryDetailsArrayList.add(inventoryDetailsList);
//         });
//       } else {
//         rs.responseBody = response;
//         rs.status = res.statusCode;
//       }
//     });

//     return inventoryDetailsArrayList;
//   }

  static Future<List<PriceCheckModel>> getPriceCheckList(
    NTLMClient client,
    String barcode,
    String locationCode,
  ) async {
    NetworkResponse rs = new NetworkResponse();
    var url = Config.priceCheckUrl;
    List<PriceCheckModel> priceCheckArrayList = new List();
    Xml2Json xml2json = new Xml2Json();
    String response = "";
    print("This is the url $url");
    await client.get(
      url,
      headers: {
        "Content-Type": "text/xml",
      },
    ).then((res) {
      print("This is the response ${res.body} ${res.statusCode}");
      var rawXmlResponse = res.body;
      xml.XmlDocument parsedXml = xml.parse(rawXmlResponse);

      if (res.statusCode == Rcode.SUCCESS_CODE) {
        parsedXml.findAllElements("PricesandOffers").forEach((val) {
          PriceCheckModel priceCheckList = new PriceCheckModel();
          // print("This is loop $val");
          xml2json.parse(val.toString());
          var json = xml2json.toParker();
          var data = jsonDecode(json);
          priceCheckList.key = data["InventoryDetails"]["Key"] ?? "";
          priceCheckList.description =
              data["InventoryDetails"]["Description"] ?? "";
          priceCheckList.discountedPrice =
              data["InventoryDetails"]["Discounted_Price"] ?? "";
          priceCheckList.offerNumber =
              data["InventoryDetails"]["offer_No"] ?? "";
          priceCheckList.salesPrice =
              data["InventoryDetails"]["Sales_Price"] ?? "";
          priceCheckList.offerType =
              data["InventoryDetails"]["Offer_TypeTxt"] ?? "";
          priceCheckList.userRef = data["InventoryDetails"]["User_Ref"] ?? "";

          priceCheckList.statusCode = res.statusCode;
          print('this is the name of the priceCheckList ');
          print(
              'this is the name of the priceCheckList ${data["priceCheckList"]["offer_No"]}');
          priceCheckArrayList.add(priceCheckList);
        });
      } else {
        rs.responseBody = response;
        rs.status = res.statusCode;
      }
    });

    return priceCheckArrayList;
  }
}
