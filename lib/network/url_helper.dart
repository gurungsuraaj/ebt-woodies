import 'package:ebt_woodies/common/constants.dart';
import 'package:ebt_woodies/utils/SpUtils.dart';
import 'package:shared_preferences/shared_preferences.dart';

class UrlHelper {
  static String getSOAPUrl(String method) {
    String server = SpUtil.getString(Constants.server);
    String port = SpUtil.getString(Constants.port);
    String service = SpUtil.getString(Constants.service);
    String web = SpUtil.getString(Constants.web);
    String company = SpUtil.getString(Constants.company);

    String url =
        "http://" + server + ":" + port + "/" + service + "/" + web + "/" + company + "/Codeunit/" + method;
    return Uri.encodeFull(url);
  }

  static String getPAGEUrl(String method) {
    String server = SpUtil.getString(Constants.server);
    String port = SpUtil.getString(Constants.port);
    String service = SpUtil.getString(Constants.service);
    String web = SpUtil.getString(Constants.web);
    String company = SpUtil.getString(Constants.company);

    String url =
        "http://" + server + ":" + port + "/" + service +  "/" + web + "/" + company + "/Page/" + method;
    return Uri.encodeFull(url);
  }

  static String getOdataUrl(String method) {
    String server = SpUtil.getString(Constants.server);
    String port = SpUtil.getString(Constants.odataPort);
    String service = SpUtil.getString(Constants.service);
    String web = SpUtil.getString(Constants.web);
    String company = SpUtil.getString(Constants.company);

    String url =
        "http://" + server + ":" + port + "/" + service +  "/" + 'OData' + "/" + "Company('$company')/"+ method;
    return Uri.encodeFull(url);
  }

}
